#include QMK_KEYBOARD_H

// clang-format off
enum layers
{
    _DEFAULT,
    _EXTENDED,
    _FUNCTION,
    _SYMBOL,
    _SUPER
};

// Tap Dance declarations
enum {
    TD_Q_TAB,
    TD_SLSH_BSLS,
};

// Tap Dance definitions
tap_dance_action_t tap_dance_actions[] = {
    // Tap once for Escape, twice for Caps Lock
    [TD_Q_TAB] = ACTION_TAP_DANCE_DOUBLE(KC_Q, KC_TAB),
    [TD_SLSH_BSLS] = ACTION_TAP_DANCE_DOUBLE(KC_SLSH, KC_BSLS),
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    // Default layer, Colemak-DH
    [_DEFAULT] = LAYOUT(
        TD(TD_Q_TAB), KC_W, KC_F, KC_P, KC_B,                                    KC_J, KC_L, KC_U, KC_Y, KC_QUOT,
        KC_A, KC_R, LALT_T(KC_S), LCTL_T(KC_T), KC_G,                            KC_M, LCTL_T(KC_N), LALT_T(KC_E), KC_I, KC_O,
        KC_Z, KC_X, KC_C, KC_D, KC_V,                                            KC_K, KC_H, KC_COMM, KC_DOT, TD(TD_SLSH_BSLS),
        LT(_EXTENDED, KC_TAB), LT(_SUPER, KC_BSPC),                              LSFT_T(KC_SPC), MO(_SYMBOL)),

    // "Extended", activate with X- --
    [_EXTENDED] = LAYOUT(
        KC_ESC, LALT(KC_LEFT), LCTL(KC_F), LALT(KC_RGHT), KC_NO,                 KC_PGUP, KC_HOME, KC_UP, KC_END, KC_DEL,
        OSM(MOD_LALT), OSM(MOD_LGUI), LCTL(KC_S), OSM(MOD_LCTL), LCTL(KC_G),     KC_PGDN, KC_LEFT, KC_DOWN, KC_RGHT, KC_ENT,
        LCTL(KC_Z), LCTL(KC_X), LCTL(KC_C), LCTL(KC_D), LCTL(KC_V),              KC_APP, RALT(KC_Q), RALT(KC_W), RALT(KC_P), KC_NO,
        KC_TRNS, KC_ENT,                                                         KC_LSFT, MO(_FUNCTION)),

    // Function layer, activate with first "extended" then X- -X
    [_FUNCTION] = LAYOUT(
        KC_MSTP, KC_MPRV, KC_MPLY, KC_MNXT, KC_TRNS,                             LCTL(KC_EQL), KC_NO, KC_NO, KC_NO, LCTL(KC_PLUS),
        OSM(MOD_LALT), OSM(MOD_LGUI), OSM(MOD_LSFT), OSM(MOD_LCTL), KC_TRNS,     KC_NO, KC_NO, KC_NO, KC_NO, LCTL(KC_MINS),
        KC_MUTE, KC_VOLD, LCTL(LSFT(KC_C)), KC_VOLU, LCTL(LSFT(KC_V)),           KC_NO, KC_NO, KC_NO, KC_NO, LCTL(KC_0),
        KC_TRNS, KC_TRNS,                                                        KC_ENT, KC_TRNS),

    // Number/symbols, activate with -- -X
    [_SYMBOL] = LAYOUT(
        KC_EXLM, KC_AT, KC_HASH, KC_DLR, KC_PERC,                                KC_EQL, KC_7, KC_8, KC_9, KC_PLUS,
        KC_SCLN, KC_COLN, KC_LCBR, KC_LPRN, KC_LBRC,                             KC_ASTR, KC_4, KC_5, KC_6, KC_MINS,
        KC_CIRC, KC_AMPR, KC_RCBR, KC_RPRN, KC_RBRC,                             KC_0, KC_1, KC_2, KC_3, KC_GRV,
        KC_TRNS, KC_NO,                                                          KC_TRNS, KC_TRNS),

    // Window/app management, activate with -X --
    [_SUPER] = LAYOUT(
        LGUI(KC_Q), LGUI(KC_W), LGUI(KC_F), LGUI(KC_SCLN), LGUI(KC_QUOT),        LGUI(KC_COMM), LGUI(KC_7), LGUI(KC_8), LGUI(KC_9), LGUI(KC_DOT),
        LGUI(KC_A), LGUI(KC_R), LGUI(KC_S), LGUI(KC_T), LGUI(KC_K),              LGUI(KC_J), LGUI(KC_4), LGUI(KC_5), LGUI(KC_6), LGUI(KC_L),
        KC_NO, LGUI(KC_X), LGUI(KC_B), LGUI(KC_D), KC_LGUI,                      LGUI(KC_M), LGUI(KC_1), LGUI(KC_2), LGUI(KC_3), LGUI(KC_H),
        LGUI(KC_TAB), KC_NO,                                                     KC_LSFT, LGUI(KC_ENT)),

};

// clang-format on

#if defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][NUM_DIRECTIONS] = {

};
#endif // defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)
