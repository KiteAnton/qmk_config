#pragma once

// Not yet available in `keymap.json` format
#ifdef MOUSEKEY_ENABLE
// The default is 100
#    define MOUSEKEY_WHEEL_INTERVAL 50
// The default is 40
#    define MOUSEKEY_WHEEL_TIME_TO_MAX 100
#endif

#define TAPPING_TERM 200
#define QUICK_TAP_TERM 150
#define PERMISSIVE_HOLD
